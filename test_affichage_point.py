import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline

#definition de la fonction y=f(4X + 5)
X = np.random.rand(100,1)
y = 5 + 4*X+np.random.randn(100,1)
#X = 2 * np.random.rand(100,1)
#y = 4 +3 * X+np.random.randn(100,1)
#Visualisation du jeu de données 
plt.plot(X,y,'b.')
plt.title('Jeu de données généré')
plt.xlabel("$x$", fontsize=10)
plt.ylabel("$y$", fontsize=10)
#plt.plot(X,4*X+ 5f,'r-')
plt.axis([0,1,0,10])
plt.show()

