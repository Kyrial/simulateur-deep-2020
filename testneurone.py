class MyNeuralNetwork:
    def __init__(self):
        self.nbLayers=0
        self.layers=[]     
        
    def info(self):
        print("Content of the network:");
        j=0;
        for i in range(len(self.layers)):
            print("Layer n° ",i," => ")
            print ("\tInput ", self.layers[i].input, 
                   "\tOutput", self.layers[i].output)             
            if (i != 0):
                print ("\tActivation Function",self.layers[i].activation_func)
                print ("\tW", self.layers[i].parameters['W'].shape,self.layers[i].parameters['W'])
                print ("\tb", self.layers[i].parameters['b'].shape,self.layers[i].parameters['b'])

            
    def addLayer(self,layer):
        self.nbLayers += 1;
        if (self.nbLayers==1): 
            # this is the first layer so adding a layer 0
            layerZero=Layer(layer.input)
            self.layers.append(layerZero)
            
        self.layers.append(layer) 
        self.layers[self.nbLayers].input=self.layers[self.nbLayers-1].output
        self.layers[self.nbLayers].output=self.layers[self.nbLayers].output
        layer.initParams()

        
        
    def set_parametersW_b (self,numlayer,matX,matb):
        self.layers[numlayer].parameters['W']=np.copy(matX)
        self.layers[numlayer].parameters['b']=np.copy(matb)
        
        
    def forward_propagation(self, X):
        #Init predictive variables for the input layer
        self.layers[0].setA(X)
        
        #Propagation for all the layers
        for l in range(1, self.nbLayers + 1):
            # Compute Z
            self.layers[l].setZ(np.dot(self.layers[l].parameters['W'],
                                       self.layers[l-1].parameters['A'])+self.layers[l].parameters['b'])
            # Applying the activation function of the layer to Z
            self.layers[l].setA(self.layers[l].activation_func(self.layers[l].parameters['Z']))
            
    
    def cost_function(self,y):            
        return (-(y*np.log(self.layers[self.nbLayers].parameters['A']+1e-8) + (1-y)*np.log( 1 - self.layers[self.nbLayers].parameters['A']+1e-8))).mean()
    
    def backward_propagation(self,y):
        #calcul de dZ dW et db pour le dernier layer
        self.layers[self.nbLayers].derivatives['dZ']=self.layers[self.nbLayers].parameters['A']-y
        self.layers[self.nbLayers].derivatives['dW']=np.dot(self.layers[self.nbLayers].derivatives['dZ'],
                                                             np.transpose(self.layers[self.nbLayers-1].parameters['A']))
        m=self.layers[self.nbLayers].parameters['A'].shape[1]#égal au nombre de colonnes de A 
        self.layers[self.nbLayers].derivatives['db']=np.sum(self.layers[self.nbLayers].derivatives['dZ'], 
                                                       axis=1, keepdims=True) / m
        
        #calcul de dZ dW db pour les autres layers
        for l in range(self.nbLayers-1,0,-1) :
            self.layers[l].derivatives['dZ']=np.dot(np.transpose(self.layers[l+1].parameters['W']),
                                            self.layers[l+1].derivatives['dZ'])*self.layers[l].backward_activation_func(self.layers[l].parameters["Z"])
            
            self.layers[l].derivatives["dW"]=np.dot(self.layers[l].derivatives['dZ'],
                                            np.transpose(self.layers[l-1].parameters['A']))
                       
            m=self.layers[l-1].parameters['A'].shape[1]#égal au nombre de colonnes de A 
            self.layers[l].derivatives['db']=np.sum(self.layers[l].derivatives['dZ'], 
                                                       axis=1, keepdims=True) / m    
            
    def update_parameters(self, eta) :
        for l in range(1,self.nbLayers+1) :
            self.layers[l].parameters['W']-=eta*self.layers[l].derivatives['dW']
            self.layers[l].parameters["b"]-=eta*self.layers[l].derivatives["db"]
            
    def convert_prob_into_class(self,probs):
        probs = np.copy(probs)#pour ne pas perdre probs, i.e. y_hat
        probs[probs > 0.5] = 1
        probs[probs <= 0.5] = 0
        return probs
    
    def plot_W_b_epoch (self,epoch,parameter_history):
        mat=[]
        max_size_layer=0
        for l in range(1, self.nbLayers+1):    
            value=parameter_history[epoch]['W'+str(l)]
            if (parameter_history[epoch]['W'+str(l)].shape[1]>max_size_layer):
                max_size_layer=parameter_history[epoch]['W'+str(l)].shape[1]
            mat.append(value)
        figure=plt.figure(figsize=((self.nbLayers+1)*3,int (max_size_layer/2)))    
        for nb_w in range (len(mat)):    
                plt.subplot(1, len(mat), nb_w+1)
                plt.matshow(mat[nb_w],cmap = plt.cm.gist_rainbow,fignum=False, aspect='auto')
                plt.colorbar()    
        thelegend="Epoch "+str(epoch)
        plt.title (thelegend)    

    def accuracy(self,y_hat, y):
        if self.layers[self.nbLayers].activation_func==softmax:
            # si la fonction est softmax, les valeurs sont sur différentes dimensions
            # il faut utiliser argmax avec axis=0 pour avoir un vecteur qui indique
            # où est la valeur maximale à la fois pour y_hat et pour y
            # comme cela il suffit de comparer les deux vecteurs qui indiquent 
            # dans quelle ligne se trouve le max
            y_hat_encoded=np.copy(y_hat)
            y_hat_encoded = np.argmax(y_hat_encoded, axis=0)
            y_encoded=np.copy(y)
            y_encoded=np.argmax(y_encoded, axis=0)
            return (y_hat_encoded == y_encoded).mean()
        # la dernière fonction d'activation n'est pas softmax.
        # par exemple sigmoid pour une classification binaire
        # il suffit de convertir la probabilité du résultat en classe
        y_hat_ = self.convert_prob_into_class(y_hat)
        return (y_hat_ == y).all(axis=0).mean()       
    
    def predict(self, x):
        self.forward_propagation(x)
        return self.layers[self.nbLayers].parameters['A']
    
    def next_batch(self,X, y, batchsize):
        # pour avoir X de la forme : 2 colonnes, m lignes (examples) et également y
        # cela permet de trier les 2 tableaux avec un indices de permutation       
        X=np.transpose(X)
        y=np.transpose(y)
        
        m=len(y)
        # permutation aléatoire de X et y pour faire des batchs avec des valeurs au hasard
        indices = np.random.permutation(m)
        X = X[indices]
        y = y[indices]
        for i in np.arange(0, X.shape[0], batchsize):
            # creation des batchs de taille batchsize
            yield (X[i:i + batchsize], y[i:i + batchsize])
    def fit(self, X, y, *args,**kwargs):    
        epochs=kwargs.get("epochs",20)
        verbose=kwargs.get("verbose",False)
        eta =kwargs.get("eta",0.01)
        batchsize=kwargs.get("batchsize",32)
    #def fit(self, X, y, epochs, eta = 0.01,batchsize=64) :
        # sauvegarde historique coût et accuracy pour affichage
        cost_history = []
        accuracy_history = []
        parameter_history = []
        for i in range(epochs):
            i+=1
            # sauvegarde des coûts et accuracy par mini-batch
            cost_batch = []
            accuracy_batch = []
            # Descente de gradient par mini-batch
            for (batchX, batchy) in self.next_batch(X, y, batchsize):
                # Extraction et traitement d'un batch à la fois
                
                # mise en place des données au bon format
                batchX=np.transpose(batchX)
                if self.layers[self.nbLayers].activation_func==softmax:
                    # la classification n'est pas binaire, y a utilisé one-hot-encoder
                    # le batchy doit donc être transposé et le résultat doit
                    # être sous la forme d'une matrice de taille batchy.shape[1]
                    
                    batchy=np.transpose(batchy.reshape((batchy.shape[0], batchy.shape[1])))
                else:
                    # il s'agit d'une classification binaire donc shape[1] n'existe
                    # pas
                    batchy=np.transpose(batchy.reshape((batchy.shape[0], 1)))
                #batchy=np.transpose(batchy.reshape((batchy.shape[0], 1)))
                self.forward_propagation(batchX)
                self.backward_propagation(batchy)
                self.update_parameters(eta)
                
                # sauvegarde pour affichage
                current_cost=self.cost_function(batchy)
                cost_batch.append(current_cost)
                y_hat = self.predict(batchX)
                current_accuracy = self.accuracy(y_hat, batchy)
                accuracy_batch.append(current_accuracy)
               
            # SaveStats on W, B as well as values for A,Z, W, b
            save_values = {}
            save_values["epoch"]=i
            for l in range(1, self.nbLayers+1):
                save_values["layer"+str(l)]=l
                save_values["Wmean"+ str(l)]=np.mean(self.layers[self.nbLayers].parameters['W'])
                save_values["Wmax"+ str(l)]=np.amax(self.layers[self.nbLayers].parameters['W'])
                save_values["Wmin"+str(l)]=np.amin(self.layers[self.nbLayers].parameters['W'])
                save_values["Wstd"+str(l)]=np.std(self.layers[self.nbLayers].parameters['W'])
                save_values["bmean"+ str(l)]=np.mean(self.layers[self.nbLayers].parameters['b'])
                save_values["bmax"+ str(l)]=np.amax(self.layers[self.nbLayers].parameters['b'])
                save_values["bmin"+str(l)]=np.amin(self.layers[self.nbLayers].parameters['b'])
                save_values["bstd"+str(l)]=np.std(self.layers[self.nbLayers].parameters['b'])
                # be careful A,Z,W and b must be copied otherwise it is a referencee
                save_values['A'+str(l)]=np.copy(self.layers[self.nbLayers].parameters['A'])
                save_values['Z'+str(l)]=np.copy(self.layers[self.nbLayers].parameters['Z'])
                save_values['W'+str(l)]=np.copy(self.layers[self.nbLayers].parameters['W'])
                save_values['b'+str(l)]=np.copy(self.layers[self.nbLayers].parameters['b'])
                
            parameter_history.append(save_values)        
            # sauvegarde de la valeur moyenne des coûts et de l'accuracy du batch pour affichage
            current_cost=np.average(cost_batch)
            cost_history.append(current_cost)
            current_accuracy=np.average(accuracy_batch)
            accuracy_history.append(current_accuracy)
        
            if(verbose == True):
                print("Epoch : #%s/%s - %s/%s - cost : %.4f - accuracy : %.4f"%(i,epochs,X.shape[1],X.shape[1], float(current_cost), current_accuracy))
              
        return self.layers, cost_history, accuracy_history, parameter_history
    