#!C:\Users\koala\AppData\Local\Programs\Python\Python37-32\python/exe
import cgitb
cgitb.enable()

print ('Content-type: text/html')
print ('')
print("<H3>Page web produite par un script Python</H3>")


import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline


def predict(X, theta0, theta1):
    return theta0 + theta1*X

def cost_function(X, y, theta0, theta1):
    m = len(y)
    total_error = 0.0
    for i in range(m):
        total_error += ((theta0 + theta1*X[i])-y[i] )**2
    return total_error / float(m)

def update_weights(X, y, theta0, theta1, eta):
    theta0_deriv = 0
    theta1_deriv = 0
    m = len(y)
    
    for i in range(m):
        # Calcul des dérivées partielles
        # -2((theta0 + theta1x) -y)
        theta0_deriv -= -2*((theta0 + theta1*X[i])-y[i])

        # -2x((theta0 + theta1x) - y)
        theta1_deriv -= -2*X[i] * ((theta0 + theta1*X[i])-y[i])
        
    # soustraction ds dérivées et multiplication par eta
    
    theta0 -= (theta0_deriv / float(m)) * eta
    theta1 -= (theta1_deriv / float(m)) * eta

    return theta0, theta1

def gradient_descent(X, y, theta0, theta1, eta=0.01, epochs=1000):
    
    #variables pour affichage à la sortie
    cost_history = []
    theta_history = np.zeros((epochs,2),dtype=float)

    for i in range(epochs):
        
        theta0,theta1 = update_weights(X, y, theta0, theta1, eta)
        # stockage des valeurs pour l'affichage
        theta_history[i][0]=theta0
        theta_history[i][1]=theta1

        cost = cost_function(X, y, theta0, theta1)
        cost_history.append(cost)

    return theta0, theta1, cost_history, theta_history



eta =0.1
epochs = 1000
X = np.random.rand(100,1)
y = 5 + 4*X+np.random.randn(100,1)
#initialisation aléatoire de theta0 et de theta1
theta0=np.random.randn()
theta1=np.random.randn()

theta0,theta1,cost_history,theta_history=gradient_descent(X, y,theta0, theta1, eta, epochs)

print ('theta0 = %0.2f'%theta0,' theta1 = %.2f'%theta1,' cost(%0.2f)'%cost_history[-1])


def plot_history (eta,epochs,cost_history):
    #fig,ax = plt.subplots(figsize=(5,5))
    fig,ax = plt.subplots(figsize=(5,5))  
    ax.set_ylabel(r'$J(\theta)$')
    ax.set_xlabel('Epochs')
    ax.set_title(r"$\eta$ :{}".format(eta))
    ax.plot(range(epochs),cost_history,'r.')
    
    

#Visualisation du jeu de données 
plt.plot(X,y,'b.')
plt.title('Jeu de données généré')
plt.xlabel("$x$", fontsize=10)
plt.ylabel("$y$", fontsize=10)
#plt.plot(X,4*X+ 5,'r-')
plt.axis([0,1.1,0,11])





plot_history(eta,epochs,cost_history)

def plot_lines (eta,epochs,theta_history):
    tr =0.1
    fig,ax = plt.subplots(figsize=(6,5))
    ax.set_xlabel("$x$", fontsize=10)
    ax.set_ylabel("$y$", fontsize=10)
    ax.plot(X,y,'b.')
    for i in range (epochs):
        pred=predict(X, theta_history[i][0], theta_history[i][1])
        if ((i % 25 == 0) ):
            _ = ax.plot(X,pred,'r-',alpha=tr)
            if tr < 0.8:
                tr = tr+0.2   
                
plot_lines(eta,epochs,theta_history)

plt.show()

